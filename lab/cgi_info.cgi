#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cgi
import cgitb
cgitb.enable()
import os
import datetime
import time


print "Content-Type: text/html"

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Zadanie przyszlo o %s.<br>
    </body>
</html>""" % (
        os.environ.get('SERVER_NAME','brak'), # nazwa serwera
        os.environ.get('SERVER_ADDR','brak'), # IP serwera
        os.environ.get('SERVER_PORT','brak'), # port serwera
        os.environ.get('REMOTE_HOST','brak'), # nazwa klienta
        os.environ.get('REMOTE_ADDR','brak'), # IP klienta
        os.environ.get('REMOTE_PORT','brak'), # port klienta
        os.environ.get('SCRIPT_NAME','brak'), # nazwa skryptu
        str(time.time.now()), # bieżący czas


)


print body,