# -*- coding: utf-8 -*-

import socket
import unittest
import requests


class SuperTesty(unittest.TestCase):

    def setUp(self):
        """ustawienia dla testow"""
        #self.logger = LoggerStub()

    def HttpGetRequest(self, qs):
        url = 'http://194.29.175.240/~p8/wsgi' + qs
        session = requests.session()
        response = session.get(url)
        session.close()
        return response.content


    def test_incorrect_querystring(self):
        answer = self.HttpGetRequest('?test=1')
        expected = '<html><head></head><body>400 BAD REQUEST / Nieprawidlowy format zapytania</body></html>'
        self.assertEqual(answer, expected)

    def test_incorrect_id(self):
        answer = self.HttpGetRequest('?id=7')
        expected = '<html><head></head><body><h3>Nie znaleziono ksiazki o takim id.</h3></body></html>'
        self.assertEqual(answer,expected)

    def test_correct_id(self):
        answer = self.HttpGetRequest('?id=id3')
        expected = '<html><head></head><body>' \
                   'Title: Foundations of Python Network Programming</br>' \
                   'ISBN: 978-1430230038</br>' \
                   'Publisher: Apress; 2 edition (December 21, 2010)</br>' \
                   'Author: John Goerzen</body></html>'
        self.assertEqual(answer, expected)

    def test_main_page(self):
        answer = self.HttpGetRequest('')
        expected = '<html><head></head><body><ul><li>' \
                   '<a href="/~p8/wsgi?id=id4">Python Cookbook, Second Edition</a></li><li>' \
                   '<a href="/~p8/wsgi?id=id5">The Pragmatic Programmer: From Journeyman to Master</a></li><li>' \
                   '<a href="/~p8/wsgi?id=id2">Python for Software Design: How to Think Like a Computer Scientist' \
                   '</a></li><li>' \
                   '<a href="/~p8/wsgi?id=id3">Foundations of Python Network Programming</a></li><li>' \
                   '<a href="/~p8/wsgi?id=id1">CherryPy Essentials: Rapid Python Web Application Development' \
                   '</a></li></ul></body></html>'
        self.assertEqual(answer, expected)

if __name__ == '__main__':
    unittest.main()