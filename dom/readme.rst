
Praca domowa #5
===============

Stworzyć małą kilkustronową aplikację WSGI korzystając z ``bookdb.py`` jako  źródła danych.
Strona początkowa powinna wyświetlać listę książek w bazie. Każda pozycja na liście powinna
być odnośnikiem do strony ze szczegółami danej książki.

Napisać testy sprawdzające poprawność wykonania aplikacji.
