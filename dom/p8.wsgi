#!/usr/bin/python
# -*- coding: utf-8 -*-

from bookdb import BookDB
import urlparse


# Główny obiekt aplikacji.
# Używając mod_wsgi trzeba go nazwać "application"
def application(environ, start_response):

    ksiazki = BookDB()
    # Budowa ciała odpowiedzi
    #response_body = 'Metoda żądania to: %s<br>' % environ['REQUEST_METHOD']
    #query_string = 'Query string to: %s' % environ['QUERY_STRING']
    response_body = ''
    status = ''
    wstep_html = '<html><head></head><body>'
    koniec_html = '</body></html>'

    #pobranie query stringa
    query_string = environ['QUERY_STRING']
    lista_ksiazek = ksiazki.titles()

    dostepne_id = []

    #wypelnij tablice dostepnymi id ksiazek
    for ksiazka in lista_ksiazek:
        if ksiazka['id'] not in dostepne_id:
            dostepne_id.append(ksiazka['id'])

    #wypisz liste ksiazek w postaci linkow
    if len(query_string) is 0:
        response_body += '<ul>'
        for ksiazka in lista_ksiazek:
            response_body += '<li><a href="/~p8/wsgi?id=' + ksiazka['id'] + '">' + ksiazka['title'] + '</a></li>'

        response_body += '</ul>'

        # Status HTTP
        status = '200 OK'
    else:
        zmienne_qs = urlparse.parse_qs(query_string)

        #czy istnieje w zapytaniu zmienna 'id', jesli tak, to:
        if zmienne_qs.__contains__('id'):

            ksiazka_id = zmienne_qs['id'][0]

            #jesli jest ksiazka z takim id
            if ksiazka_id in dostepne_id:
                k = ksiazki.title_info(ksiazka_id)
                response_body += 'Title: ' + k['title'] + '</br>'
                response_body += 'ISBN: ' + k['isbn'] + '</br>'
                response_body += 'Publisher: ' + k['publisher'] + '</br>'
                response_body += 'Author: ' + k['author']
                status = '200 OK'
            else:
                status = '200 OK'
                #response_body += ksiazka_id
                response_body += '<h3>Nie znaleziono ksiazki o takim id.</h3>'
        else:
            response_body += '400 BAD REQUEST / Nieprawidlowy format zapytania'
            status = '400 BAD REQUEST'



    # Nagłówki HTTP oczekiwane przez klienta.
    # Powinny być podane w postaci listy zawierającej dwuelementowe krotki
    # [(Nazwa nagłówka, Wartość nagłówka)].
    response_headers = [('Content-Type', 'text/html; charset=utf-8'),
                        ('Content-Length', str(len(response_body) + len(wstep_html) + len(koniec_html)))]

    # Wysłanie ich do serwera przy użyciu dostarczonej metody (callback)
    start_response(status, response_headers)

    # Zwrot ciała odpowiedzi
    # Opakowane w listę, ale może to być dowolny obiekt typu iterable
    return [wstep_html, response_body, koniec_html]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 9889, application)
    srv.serve_forever()